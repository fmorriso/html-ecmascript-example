"use strict";

export class Greeting {
    // use of # makes a variable private in EcmaScript 2022 and newer
    #count = 0;
    #greeting = ''


    constructor(greet) {
        this.#greeting = greet;
    }

    get greeting() {
        return this.#greeting.toUpperCase();
    }

    set greeting(greet) {
        this.#greeting = greet;
    }

    get count() {
        return this.#count;
    }

    // use of # makes the method private
    #increaseCount() {
        this.#count++;
    }

    showGreeting() {
        const msg = `Welcome and ${this.greeting}!`;
        console.log(msg);
        alert(msg)
        this.#increaseCount();
    }

    showCount() {
        console.log(`count: ${this.count}`);
    }

}
