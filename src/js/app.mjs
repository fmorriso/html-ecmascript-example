"use strict";

// NOTE: the file extension MUST be .mjs in order for the
//       import of Ecmascript to be properly recognized by "import".
import {Greeting} from './greeting.mjs';

export async function afterPageLoaded() {
    console.log('top of afterPageLoaded');    
    const greet1 = new Greeting('Good afternoon');  
    // wait for one-half second  
    await sleep(500);
    greet1.showGreeting();
    greet1.showCount();
}

async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

